FROM openjdk:8u121-alpine
MAINTAINER stpork from Mordor team

ENV BAMBOO_AGENT_VERSION	6.2.2
ENV BAMBOO_AGENT_INSTALL	/opt/atlassian/bamboo-agent
ENV BAMBOO_AGENT_HOME		/var/atlassian/application-data/bamboo-agent
ENV BAMBOO_AGENT_USER		daemon
ENV BAMBOO_AGENT_GROUP		daemon
ENV BAMBOO_AGENT_JAR		atlassian-bamboo-agent-installer-${BAMBOO_AGENT_VERSION}.jar
ENV BAMBOO_AGENT_SERVER		http://bamboo:8085/agentServer/

ENV HOME					$BAMBOO_AGENT_HOME/home
ENV M2_HOME					$HOME/.m2
ENV _JAVA_OPTIONS			-Duser.home=$HOME

ARG AGENT_URL=https://bitbucket.org/stpork/bamboo-agent/downloads/${BAMBOO_AGENT_JAR}
ARG M2_URL=https://bitbucket.org/stpork/bamboo-agent/downloads/settings.xml
ARG OC_URL=http://github.com/openshift/origin/releases/download/v3.6.1/openshift-origin-client-tools-v3.6.1-008f2d5-linux-64bit.tar.gz
ARG CLI_URL=http://bobswift.atlassian.net/wiki/download/attachments/16285777/atlassian-cli-7.2.0-distribution.zip

RUN apk update -qq \
	&& update-ca-certificates \
	&& apk add ca-certificates wget curl git libc6-compat openssh bash procps openssl perl ttf-dejavu tini maven nano xmlstarlet \
	&& rm -rf /var/lib/{apt,dpkg,cache,log}/ /tmp/* /var/tmp/* \
	&& mkdir /lib64 \
	&& ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2 \
	&& mkdir -p ${BAMBOO_AGENT_INSTALL} \
	&& curl -o ${BAMBOO_AGENT_INSTALL}/${BAMBOO_AGENT_JAR} -L --silent ${AGENT_URL} \
	&& mkdir -p ${M2_HOME} \
	&& curl -o ${M2_HOME}/settings.xml -L --silent ${M2_URL} \
	&& curl -L --silent ${OC_URL} | tar -xz --strip-components=1 -C "/usr/local/bin" \
	&& cd /opt \
	&& curl -o atlassian-cli.zip -L --silent ${CLI_URL} \
	&& unzip atlassian-cli.zip \
	&& mv atlassian-cli-7.1.0/* "/usr/local/bin" \
	&& rm -rf atlassian-cli* \
	&& chown -R ${BAMBOO_AGENT_USER}:${BAMBOO_AGENT_GROUP} /usr/local/bin \
	&& chmod -R 777 /usr/local/bin \
	&& chown -R ${BAMBOO_AGENT_USER}:${BAMBOO_AGENT_GROUP} ${BAMBOO_AGENT_INSTALL} \
	&& chmod -R 777 ${BAMBOO_AGENT_INSTALL} \
	&& chown -R ${BAMBOO_AGENT_USER}:${BAMBOO_AGENT_GROUP} ${BAMBOO_AGENT_HOME} \ 
	&& chmod -R 777 ${BAMBOO_AGENT_HOME}

USER ${BAMBOO_AGENT_USER}:${BAMBOO_AGENT_GROUP}

EXPOSE 8085
EXPOSE 54663

VOLUME ["${BAMBOO_AGENT_HOME}"]

WORKDIR ${BAMBOO_AGENT_INSTALL}

COPY entrypoint.sh /entrypoint.sh

CMD ["/entrypoint.sh"]
ENTRYPOINT ["/sbin/tini", "--"]
