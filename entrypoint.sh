#!/bin/bash
set -euo pipefail

# Set recommended umask of "u=,g=w,o=rwx" (0027)
umask 0027

# Setup Catalina Opts
: ${CATALINA_CONNECTOR_PROXYNAME:=}
: ${CATALINA_CONNECTOR_PROXYPORT:=}
: ${CATALINA_CONNECTOR_SCHEME:=http}
: ${CATALINA_CONNECTOR_SECURE:=false}

: ${CATALINA_OPTS:=}

: ${JAVA_OPTS:=}

: ${ELASTICSEARCH_ENABLED:=true}
: ${APPLICATION_MODE:=}

CATALINA_OPTS="${CATALINA_OPTS} -DcatalinaConnectorProxyName=${CATALINA_CONNECTOR_PROXYNAME}"
CATALINA_OPTS="${CATALINA_OPTS} -DcatalinaConnectorProxyPort=${CATALINA_CONNECTOR_PROXYPORT}"
CATALINA_OPTS="${CATALINA_OPTS} -DcatalinaConnectorScheme=${CATALINA_CONNECTOR_SCHEME}"
CATALINA_OPTS="${CATALINA_OPTS} -DcatalinaConnectorSecure=${CATALINA_CONNECTOR_SECURE}"

JAVA_OPTS="${JAVA_OPTS} ${CATALINA_OPTS}"

ARGS="$@"

# Start Bamboo as the correct user.
if [ "${UID}" -eq 0 ]; then
    echo "User is currently root. Will change directory ownership to ${BAMBOO_AGENT_USER}:${BAMBOO_AGENT_GROUP}, then downgrade permission to ${BAMBOO_AGENT_USER}"
    PERMISSIONS_SIGNATURE=$(stat -c "%u:%U:%a" "${BAMBOO_AGENT_HOME}")
    EXPECTED_PERMISSIONS=$(id -u ${BAMBOO_AGENT_USER}):${BAMBOO_AGENT_USER}:700
    if [ "${PERMISSIONS_SIGNATURE}" != "${EXPECTED_PERMISSIONS}" ]; then
        echo "Updating permissions for BAMBOO_AGENT_HOME"
        mkdir -p "${BAMBOO_AGENT_HOME}/lib" &&
            chmod -R 700 "${BAMBOO_AGENT_HOME}" &&
            chown -R "${BAMBOO_AGENT_USER}:${BAMBOO_AGENT_GROUP}" "${BAMBOO_AGENT_HOME}"
    fi
    # Now drop privileges
    echo "Executing with downgraded permissions"
    exec su -s /bin/bash "${BAMBOO_AGENT_USER}" -c java -jar "${BAMBOO_AGENT_INSTALL}/${BAMBOO_AGENT_JAR} ${BAMBOO_AGENT_SERVER} ${ARGS}"
else
    echo "Executing with default permissions"
	ls -la /opt/atlassian/bamboo-agent/
    exec java -jar "${BAMBOO_AGENT_INSTALL}/${BAMBOO_AGENT_JAR} ${BAMBOO_AGENT_SERVER} ${ARGS}"
fi
